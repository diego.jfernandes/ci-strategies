#!/bin/bash

TIME="30"
URL="https://api.telegram.org/bot$BOT_TOKEN/sendMessage"
PASS="Sucesso, os testes passaram! ✅🚀✨%0A%0ABranch:+$CI_COMMIT_REF_SLUG%0AStage:+$CI_JOB_STAGE%0AAutomação:+$CI_PROJECT_NAME%0APlataforma:+$CI_JOB_NAME%0AInciado por:+$GITLAB_USER_LOGIN%0AJob:+$CI_JOB_URL%0ARelatório:+https://$GITLAB_USER_LOGIN.gitlab.io/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/cypress/results/report.html"
FAIL="Os testes falharam!!! ❌🐞🐛%0A%0ABranch:+$CI_COMMIT_REF_SLUG%0AStage:+$CI_JOB_STAGE%0AAutomação:+$CI_PROJECT_NAME%0APlataforma:+$CI_JOB_NAME%0AInciado por:+$GITLAB_USER_LOGIN%0AJob:+$CI_JOB_URL%0ARelatório:+https://$GITLAB_USER_LOGIN.gitlab.io/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/cypress/results/report.html"

# curl -s --max-time $TIME -d "chat_id=-1001583238231&disable_web_page_preview=1&text=$PASS" $URL > /dev/null

if [ "$CI_JOB_STATUS" = "success" ]; then
  curl -s --max-time $TIME -d "chat_id=857591756&disable_web_page_preview=1&text=$PASS" $URL > /dev/null
  curl -s -X POST \
    -H 'Content-type: application/json; charset=utf-8' \
    -d '{"channel":"#outros-assuntos", "username":"jarvis", "attachments":[{"color": "#2ECC71", "blocks":[{"type":"section","text":{"type":"mrkdwn","text": "*Sucesso!* :white_check_mark: :rocket:\n\n *Stage:* '$CI_JOB_STAGE'\n*Branch:* '$CI_COMMIT_REF_SLUG'\n*Automação:* '$CI_PROJECT_NAME'\n *Plataforma:* '$CI_JOB_NAME'\n *Inciado por:* '$GITLAB_USER_LOGIN'\n\n <'$CI_JOB_URL'|*Logs do job*>\n\n <'$CI_JOB_URL'/artifacts/file/cypress/results/report.html|*Relatório do teste*>"}}]}]}' \
    https://hooks.slack.com/services/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
else
  curl -s --max-time $TIME -d "chat_id=857591756&disable_web_page_preview=1&text=$FAIL" $URL > /dev/null
  curl -s -X POST \
    -H 'Content-type: application/json; charset=utf-8' \
    -d '{"channel":"#outros-assuntos", "username":"jarvis", "attachments":[{"color": "#C0392B", "blocks":[{"type":"section","text":{"type":"mrkdwn","text": "*Falha!* :x: :ladybug: :bug:\n\n *Stage:* '$CI_JOB_STAGE'\n *Branch:* '$CI_COMMIT_REF_SLUG'\n *Automação:* '$CI_PROJECT_NAME'\n *Plataforma:* '$CI_JOB_NAME'\n *Inciado por:* '$GITLAB_USER_LOGIN'\n\n <'$CI_JOB_URL'|*Logs do job*>\n\n <'$CI_JOB_URL'/artifacts/file/cypress/results/report.html|*Relatório do teste*>"}}]}]}' \
    https://hooks.slack.com/services/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 
fi
