const { defineConfig } = require("cypress");
const db = require("./cypress/support/db/db-hml");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      on("task", {
        queryDb: (query) => {
          return db.queryTestDb(query, config);
        },
      });
    },
    baseUrl: "https://www.google.com.br",
    specPattern: "**/test/*.cy.js",
    chromeWebSecurity: true,
    reporter: "mochawesome",
    reporterOptions: {
      reportFilename: "[status]_[datetime]-[name]-report",
      timeStamp: "longDate",
      reportDir: "cypress/results/reports",
      overwrite: false,
      html: false,
      json: true,
      embeddedScreenshots: true,
      inlineAssets: true,
    },
    env:{

    }
  },
});
