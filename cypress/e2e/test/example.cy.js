describe('Suite teste Google', () => {
  beforeEach(() => {
    cy.visit('/')
  });
  
  it('Verify title', () => {
    cy.title().then((title) => {
      expect(title).be.eq('Google')
    })
    cy.title().should('include', 'Google')
  });

  it('Search term', () => {
    const term = "economia"
    cy.get('.gLFyf').type(term)
  });

  it('Click have luck', () => {
    const term = "economia"
    cy.get('.gLFyf').type(term)
    cy.get('.CqAVzb > center > .RNmpXc').contains('Estou com sorte').should('be.visible')
    
  })

  it.only('create table mysql', () => {
    // cy.createTable()
    cy.showTables()
  });
})