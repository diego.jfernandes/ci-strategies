Cypress.Commands.add('createTable', () => { 
  cy.task("queryDb", `CREATE TABLE cliente (PersonID int, LastName varchar(255), FirstName varchar(255), Address varchar(255), City varchar(255));`).then((result) => {
    console.log(result)
  })
})

Cypress.Commands.add('showTables', () => { 
  cy.task("queryDb", `SHOW TABLES`).then((result) => {
    console.log(result)
  })
})
