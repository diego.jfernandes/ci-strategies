const mysql = require('mysql')

function queryTestDb(query, config) {
 
  const connection = mysql.createConnection(config.env)

  connection.connect()

  return new Promise((resolve, reject) => {
    connection.query(query, (error, results) => {
      if(error) reject(error);
      else {
        console.log('Conexão estabelecida!')
        connection.end();
        return resolve(results);
      }
    })
  })
}

module.exports = {queryTestDb}